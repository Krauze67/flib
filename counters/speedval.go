package counters

import (
	"sync/atomic"
)

type SpeedVal struct {
	nAmount int64
	nTimeNs int64

	//lock sync.RWMutex
}

func (t *SpeedVal) Amount() int64 {
	return t.nAmount
}

func (t *SpeedVal) Update(amount int64, periodNanoSec int64) {
	atomic.AddInt64(&t.nAmount, amount)
	atomic.AddInt64(&t.nTimeNs, periodNanoSec)
}

func (t *SpeedVal) PerNanoSec() float64 {
	return t.valPerPeriod(1)
}

func (t *SpeedVal) PerMicroSec() float64 {
	return t.valPerPeriod(1000)
}

func (t *SpeedVal) PerMilliSec() float64 {
	return t.valPerPeriod(1000000)
}

func (t *SpeedVal) PerSec() float64 {
	return t.valPerPeriod(1000000000)
}

func (t *SpeedVal) PerMin() float64 {
	return t.PerSec() * 60
}

func (t *SpeedVal) valPerPeriod(perioddiv float64) float64 {
	divider := (float64(t.nTimeNs) / perioddiv)
	if 0 == t.nTimeNs || 0 == divider {
		return float64(t.nAmount)
	}
	return float64(t.nAmount) / divider
}
