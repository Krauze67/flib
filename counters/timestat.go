package counters

import (
	"time"
)

type TimeStat struct {
	Stat      *StatVal
	StartTime time.Time
	Point     time.Duration
}

func (t TimeStat) New(pStatToUpdate *StatVal, bStartNow bool, calc_points time.Duration) *TimeStat {
	if nil == pStatToUpdate {
		panic("nil pointer to stat value")
	}
	ret := &TimeStat{}
	ret.Stat = pStatToUpdate
	ret.Point = calc_points
	if bStartNow {
		ret.Start()
	}

	return ret
}

func (t *TimeStat) Start() {
	if nil != t.Stat && t.StartTime.IsZero() {
		t.StartTime = time.Now()
	}
}

func (t *TimeStat) IsStarted() bool {
	return !t.StartTime.IsZero()
}

func (t *TimeStat) Stop() {
	if nil != t.Stat && !t.StartTime.IsZero() {
		timeStop := time.Now()
		if t.StartTime.After(timeStop) {
			panic("Invalid start time")
		}
		t.Stat.Update(uint64(timeStop.Sub(t.StartTime).Nanoseconds() / t.Point.Nanoseconds()))
		t.StartTime = time.Time{} //to be able to start again
	}
}
