package counters

import (
	"sync"
	"sync/atomic"
)

type StatVal struct {
	nAmount_Total     uint64
	nAmount_Min       uint64
	nAmount_Max       uint64
	nOperations_Count uint64

	lock sync.RWMutex
}

func (t *StatVal) New() *StatVal {
	return &StatVal{}
}

func (t *StatVal) Update(amount uint64) {
	if nil == t {
		panic("nil pointer to stat value to update")
	}

	t.lock.Lock()
	defer t.lock.Unlock()

	nowCount := atomic.AddUint64(&t.nOperations_Count, 1)

	atomic.AddUint64(&t.nAmount_Total, amount)

	if amount > t.nAmount_Max || 1 == nowCount {
		atomic.SwapUint64(&t.nAmount_Max, amount)
	}

	if amount < t.nAmount_Min || 1 == nowCount {
		atomic.SwapUint64(&t.nAmount_Min, amount)
	}

}

func (t *StatVal) Min() uint64 {
	t.lock.RLock()
	defer t.lock.RUnlock()
	return t.nAmount_Min
}

func (t *StatVal) Max() uint64 {
	t.lock.RLock()
	defer t.lock.RUnlock()
	return t.nAmount_Max
}

func (t *StatVal) Avg() float64 {
	t.lock.RLock()
	defer t.lock.RUnlock()
	ret := float64(t.nAmount_Total)
	if t.nOperations_Count > 0 {
		ret = float64(t.nAmount_Total / t.nOperations_Count)
	}
	return ret
}

func (t *StatVal) Count() uint64 {
	t.lock.RLock()
	defer t.lock.RUnlock()
	return t.nOperations_Count
}
