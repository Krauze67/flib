package counters

import (
	"sync/atomic"
	"time"
)

func AddLinearInt64(pVal *int64, val int64) {
	if nil == pVal {
		panic("nil pointer to value to update")
	}

	atomic.AddInt64(pVal, val)
}

func MoveFromToInt64(pValFrom *int64, pValTo *int64, val int64) {
	AddLinearInt64(pValFrom, -val)
	AddLinearInt64(pValTo, val)
}

// To subtract a signed positive constant value c from x, do
// AddUint64(&x, ^uint64(c-1)).
// In particular, to decrement x, do
// AddUint64(&x, ^uint64(0)).
func AddLinearUint64(pVal *uint64, val uint64) {
	if nil == pVal {
		panic("nil pointer to value to update")
	}

	atomic.AddUint64(pVal, val)
}

func DecrementLinearUint64(pVal *uint64) {
	AddLinearUint64(pVal, ^uint64(0))
}

func MoveFromToUint64(pValFrom *uint64, pValTo *uint64, val uint64) {
	AddLinearUint64(pValFrom, ^uint64(val-1))
	AddLinearUint64(pValTo, val)
}

func UpdateSpeedCounter(pVal *SpeedVal, period_begin time.Time, period_end time.Time, amount int64) {
	if nil == pVal {
		panic("nil pointer to speed value to update")
	}

	if period_begin.After(period_end) {
		panic("invalid speed period")
	}

	pVal.Update(amount, int64(period_end.Sub(period_begin).Nanoseconds()))
}

func SetSpeedCounter(pVal *SpeedVal, period_begin time.Time, period_end time.Time, amount int64) {
	if nil == pVal {
		panic("nil pointer to speed value to update")
	}

	if period_begin.After(period_end) {
		panic("invalid speed period")
	}

	atomic.SwapInt64(&pVal.nAmount, amount)
	atomic.SwapInt64(&pVal.nTimeNs, int64(period_end.Sub(period_begin).Nanoseconds()))
}
