package counters

import (
	"sync"
	"sync/atomic"
	"time"
)

type SpeedMeter struct {
	pSpeedToUpdate *SpeedVal
	nStartAmount   int64
	timeStart      time.Time

	lock sync.Mutex
}

func (t SpeedMeter) GetWrapSpeed(speedval *SpeedVal, startNow bool) *SpeedMeter {
	if nil == speedval {
		panic("nil pointer to speed value")
	}

	ret := &SpeedMeter{}
	ret.pSpeedToUpdate = speedval
	if startNow {
		t.Start()
	}

	return ret
}

func (t SpeedMeter) GetNew(startNow bool) *SpeedMeter {

	ret := &SpeedMeter{}
	ret.pSpeedToUpdate = &SpeedVal{}

	if startNow {
		ret.Start()
	}
	return ret
}

func (t *SpeedMeter) Speed() *SpeedVal {
	return t.pSpeedToUpdate
}

func (t *SpeedMeter) Start() {
	t.lock.Lock()
	defer t.lock.Unlock()
	if nil == t.pSpeedToUpdate {
		panic("nil pointer to speed value")
	}
	if !t.timeStart.IsZero() {
		return
	}
	startAmount := t.pSpeedToUpdate.Amount()
	t.nStartAmount = startAmount
	t.timeStart = time.Now()
}

func (t *SpeedMeter) StartWithAmount(startAmount int64) {
	t.lock.Lock()
	defer t.lock.Unlock()
	if nil == t.pSpeedToUpdate {
		panic("nil pointer to speed value")
	}
	if !t.timeStart.IsZero() {
		return
	}
	t.timeStart = time.Now()
	t.nStartAmount = startAmount
}

func (t *SpeedMeter) intUpdateDelta(amount_since_last_update int64) {
	if nil == t.pSpeedToUpdate {
		panic("nil pointer to speed value")
	}
	if t.timeStart.IsZero() {
		panic("not started yet")
	}

	now := time.Now()
	UpdateSpeedCounter(t.pSpeedToUpdate, t.timeStart, now, amount_since_last_update)

	t.timeStart = now
	t.nStartAmount += amount_since_last_update
}

func (t *SpeedMeter) UpdateAmount(current_amount int64) *SpeedMeter {
	t.lock.Lock()
	defer t.lock.Unlock()
	if nil == t.pSpeedToUpdate {
		panic("nil pointer to speed value")
	}
	if t.timeStart.IsZero() {
		panic("not started yet")
	}

	t.intUpdateDelta(current_amount - t.nStartAmount)
	return t
}

func (t *SpeedMeter) UpdateDelta(amount_since_last_update int64) {
	t.lock.Lock()
	defer t.lock.Unlock()
	t.intUpdateDelta(amount_since_last_update)
}

func (t *SpeedMeter) UpdateDeltaByExtCounter(pAmount_since_last_update *int64) {
	t.lock.Lock()
	defer t.lock.Unlock()
	if nil == t.pSpeedToUpdate {
		panic("nil pointer to speed value")
	}
	if nil == pAmount_since_last_update {
		panic("nil pointer to update value")
	}
	if t.timeStart.IsZero() {
		panic("not started yet")
	}

	t.intUpdateDelta(*pAmount_since_last_update)
}

func (t *SpeedMeter) UpdateDeltaAndResetExtCounter(pAmount_since_last_update *int64) {
	t.lock.Lock()
	defer t.lock.Unlock()
	if nil == t.pSpeedToUpdate {
		panic("nil pointer to speed value")
	}
	if nil == pAmount_since_last_update {
		panic("nil pointer to update value")
	}
	if t.timeStart.IsZero() {
		panic("not started yet")
	}

	t.intUpdateDelta(*pAmount_since_last_update)
	atomic.StoreInt64(pAmount_since_last_update, 0)
}

//func (t *SpeedMeter) Reset(bStartAgain bool) {
//	if nil == t.pSpeedToUpdate {
//		panic("nil pointer to speed value")
//	}
//	if t.timeStart.IsZero() {
//		panic("not started yet")
//	}

//	t.Stop()
//	if bStartAgain {
//		t.Start()
//	}
//}

//func (t *SpeedMeter) ResetWithAmount(bStartAgain bool, startAmount uint64) {
//	if nil == t.pSpeedToUpdate {
//		panic("nil pointer to speed value")
//	}
//	if t.timeStart.IsZero() {
//		panic("not started yet")
//	}

//	t.Stop()
//	if bStartAgain {
//		t.StartWithAmount(startAmount)
//	}
//}
