package httpclient

import (
	"bytes"
	"compress/gzip"
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"net/http/cookiejar"
	"strings"

	"gitlab.com/Krauze67/flib/net/proxy"

	"time"

	"errors"

	"strconv"

	log "github.com/sirupsen/logrus"
)

const (
	httpclient_cDefConnectTimeout = 10
	httpclient_cDefLoadTimeout    = 30

	httpclient_cHdrNameContentEncoding = "Content-Encoding"
	httpclient_cHdrValGzip             = "gzip"
)

// Client wraps net/http to easy use
type Client struct {
	proxyHost   string
	proxyPort   int
	connTimeout time.Duration

	client    *http.Client
	transport *http.Transport

	Url       string
	Request   *http.Request
	ReqHeader http.Header

	Response   *http.Response
	RespHeader http.Header
	StatusCode int
	Body       []byte
}

// GetNewClient returns a new client with no proxy and connection timeout set
func GetNewClient(conntimeoutsec uint, pDefHeaders *http.Header) *Client {
	retval := &Client{
		connTimeout: time.Duration(httpclient_cDefConnectTimeout) * time.Second,
		ReqHeader:   http.Header{},
	}

	if conntimeoutsec > 0 {
		retval.connTimeout = time.Duration(conntimeoutsec) * time.Second
	}

	if nil != pDefHeaders {
		retval.ReqHeader = *pDefHeaders
	}
	retval.SetProxy("", 0, retval.connTimeout)
	return retval
}

// GetNewProxiedClient returns a new client with proxy and connection timeout set
func GetNewProxiedClient(proxyhost string, proxyport int, conntimeoutsec uint, pDefHeaders *http.Header) *Client {
	retval := &Client{
		connTimeout: time.Duration(httpclient_cDefConnectTimeout) * time.Second,
		ReqHeader:   http.Header{},
	}

	if conntimeoutsec > 0 {
		retval.connTimeout = time.Duration(conntimeoutsec) * time.Second
	}

	if nil != pDefHeaders {
		retval.ReqHeader = *pDefHeaders
	}
	retval.SetProxy(proxyhost, proxyport, retval.connTimeout)
	return retval
}

// GetProxy returns a full proxy address
func (t *Client) GetProxy() string {
	if nil == t {
		panic("Client Member function called with nil 'this'")
	}
	if 0 == len(t.proxyHost) || 0 >= t.proxyPort {
		return ""
	}
	return fmt.Sprintf("%s:%v", t.proxyHost, t.proxyPort)
}

// SetHeader sets additional/removes header
func (t *Client) SetHeader(key string, val string) {
	if nil == t {
		panic("Client Member function called with nil 'this'")
	}
	t.ReqHeader.Set(key, val)
}

// SetProxy sets a new proxy address
func (t *Client) SetProxy(proxyhost string, proxyport int, conntimeout time.Duration) bool {
	if nil == t {
		panic("Client Member function called with nil 'this'")
	}
	t.proxyHost = proxyhost
	t.proxyPort = proxyport
	proxyandport := ""
	if 0 < len(t.proxyHost) {
		proxyandport = fmt.Sprintf("%s:%v", t.proxyHost, t.proxyPort)
	}

	return t.SetProxyAndPort(proxyandport, conntimeout)
}

func (t *Client) SetProxyAndPort(proxyandport string, conntimeout time.Duration) bool {
	if nil == t {
		panic("Client Member function called with nil 'this'")
	}

	//proxyUrl, _ := url.Parse("http://127.0.0.1:8888")//Fiddler

	t.transport = &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		Dial: (&net.Dialer{
			Timeout: conntimeout,
		}).Dial,
		//Proxy:               http.ProxyURL(proxyUrl),//Fiddler
		TLSHandshakeTimeout: conntimeout,
	}

	// create proxied dialer if required
	//if false { // if 0 < len(proxyandport) {  //Fiddler
	if 0 < len(proxyandport) {
		parts := strings.Split(proxyandport, ":")
		if 2 != len(parts) {
			return false
		}
		t.proxyHost = parts[0]
		var err error
		t.proxyPort, err = strconv.Atoi(parts[1])
		if nil != err {
			return false
		}

		// create a socks5 dialer
		proxiedDialer := proxy.SOCKS5("tcp", proxyandport, nil, proxy.Direct, conntimeout)
		t.transport.Dial = proxiedDialer.Dial
	}

	if nil == t.client {
		t.client = &http.Client{}
	}
	// All users of cookiejar should import "golang.org/x/net/publicsuffix"
	jar, _ := cookiejar.New(nil)
	// setup a transport
	t.client.Transport = t.transport
	t.client.Timeout = httpclient_cDefLoadTimeout
	t.client.Jar = jar

	return true
}

func (t *Client) addHeaders(preq *http.Request, ph *map[string]string) {
	for hn, hv := range *ph {
		preq.Header.Set(hn, hv)
	}
}

// Get returns: HTTPcode int, Body []byte, response Header, Error
func (t *Client) Get(url string, loadtimeout time.Duration, pAddHeaders *map[string]string) (int, []byte, http.Header, error) {
	return t.CustomRequest(http.MethodGet, url, nil, loadtimeout, pAddHeaders)
}

// Post returns: HTTPcode int, Body []byte, response Header, Error
func (t *Client) Post(url string, loadtimeout time.Duration, postdata []byte, pAddHeaders *map[string]string) (int, []byte, http.Header, error) {
	return t.CustomRequest(http.MethodPost, url, postdata, loadtimeout, pAddHeaders)
}

// CustomRequest returns: HTTPcode int, Body []byte, response Header, Error
func (t *Client) CustomRequest(method string, url string, postdata []byte, loadtimeout time.Duration, pAddHeaders *map[string]string) (int, []byte, http.Header, error) {
	if nil == t {
		panic("Client Member function called with nil 'this'")
	}

	t.Url = url

	if loadtimeout > 0 {
		t.client.Timeout = loadtimeout
	}

	var err error
	if nil != postdata {
		t.Request, err = http.NewRequest(method, t.Url, bytes.NewBuffer(postdata))
	} else {
		t.Request, err = http.NewRequest(method, t.Url, nil)
	}
	if nil != err {
		return 0, nil, http.Header{}, fmt.Errorf("Error creating new request. url[%s]:%v", url, err.Error())
	}
	t.Request.Header = t.ReqHeader
	if nil != pAddHeaders {
		t.addHeaders(t.Request, pAddHeaders)
	}

	t.Response, err = t.client.Do(t.Request)
	if nil != err {
		return 0, nil, http.Header{}, fmt.Errorf("Error requesting url[%s]:%v", url, err.Error())
	}
	defer t.Response.Body.Close()

	t.StatusCode = t.Response.StatusCode
	t.RespHeader = t.Response.Header

	//unzip body if needed:
	reader := t.Response.Body
	if hdr, ok := t.RespHeader[httpclient_cHdrNameContentEncoding]; ok {
		if strings.Contains(hdr[0], httpclient_cHdrValGzip) {
			readerGzip, err := gzip.NewReader(t.Response.Body)
			if nil == err {
				reader = readerGzip
				defer reader.Close()
			}
		}
	}

	// switch response.Header.Get("Content-Encoding") {
	// case "gzip":
	// 	reader, err = gzip.NewReader(response.Body)
	// 	defer reader.Close()
	// default:
	// 	reader = response.Body
	// }

	t.Body, err = ioutil.ReadAll(reader)
	if err != nil {
		return t.StatusCode, t.Body, t.RespHeader, fmt.Errorf("Url[%s]. Error reading response body:%v", url, err.Error())
	}

	return t.StatusCode, t.Body, t.RespHeader, nil
}

// SaveLastResponse saves last response to file
func (t *Client) SaveLastResponse(filename string) error {
	if nil == t {
		panic("Client Member function called with nil 'this'")
	}

	if 0 >= len(filename) {
		return errors.New("Empty file name")
	}

	//construct all content
	fullcontent := "<!-- \n\n"
	fullcontent += "Url:" + t.Url + "\n\n"
	fullcontent += "Method:" + t.Request.Method + "\n"
	fullcontent += "Request headers:\n" + t.getHeadersStr(&t.Request.Header) + "\n"
	//if "POST" == t.Request.Method {
	//fullcontent += "\nPOST DATA:" + t.Request.Pos + "\n"
	//}

	fullcontent += "\n\nReply:\nHttpCode:" + fmt.Sprintf("%v", t.StatusCode) + "\n"
	fullcontent += "\nReply Headers:\n" + t.getHeadersStr(&t.Response.Header) + "\n"
	fullcontent += " -->\n"

	fullcontent += string(t.Body)

	err := ioutil.WriteFile(filename, []byte(fullcontent), 0666)
	if err != nil {
		log.Errorf(fmt.Sprintf("Url[%s]. Error writing body to file[%s]:%v", t.Url, filename, err.Error()))
	}
	return nil
}

func (t *Client) getHeadersStr(phdr *http.Header) string {
	//construct all content
	fullcontent := ""
	for k, v := range *phdr {
		fullcontent += k + ": "
		for i, itm := range v {
			fullcontent += itm
			if 0 < i {
				fullcontent += "; "
			}
		}
		fullcontent += "\n"
	}
	return fullcontent
}
