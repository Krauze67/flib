package httpclient

import (
	"net/http"
	"strings"
)

func MergeUserHeaders(pmh ...*map[string]string) *map[string]string {
	pout := &map[string]string{}
	for _, nextpHeadersMap := range pmh {
		for k, v := range *nextpHeadersMap {
			(*pout)[k] = v
		}
	}

	return pout
}

func GetSubstringBytes(src []byte, before string, after string) []byte {
	if nil == src || 0 == len(src) {
		return nil
	}

	startIdx := 0
	if 0 < len(before) {
		startIdx = strings.Index(string(src), before)
		if 0 > startIdx {
			return nil
		}
		startIdx += len(before)
	}

	endIdx := len(src) - 1
	if 0 < len(after) {
		if startIdx > 0 {
			endIdx = strings.Index(string(src[startIdx:]), after)
			if endIdx >= 0 { //'after' substring found
				// need to be corrected because search done on a new slice
				endIdx += startIdx
			}
		} else {
			endIdx = strings.Index(string(src), after)
		}
	}

	return src[startIdx:endIdx]
}

// GetSubstringFromHeaders returns substring of
func GetSubstringFromHeaders(hdrs http.Header, before string, after string) []byte {
	retval := make([]byte, 0)
	for hdrName, currheader := range hdrs {
		//Check header name first
		retval = GetSubstringBytes([]byte(hdrName), before, after)
		if 0 < len(retval) {
			break
		}
		//check header value
		for _, val := range currheader {
			//tmpStr := string(val)
			//log.Infof("check header val %s", tmpStr)
			retval = GetSubstringBytes([]byte(val), before, after)
			if 0 < len(retval) {
				return retval
			}
		}
	}
	return retval
}

// GetHeadersVal returns complete content of specified header(s)
func GetHeadersVal(hdrs http.Header, headername string) []byte {
	retval := make([]byte, 0)
	cntValFound := 0
	for hdrName, currheader := range hdrs {
		//Check header name
		if 0 == strings.Compare(strings.ToLower(hdrName), strings.ToLower(headername)) {
			//append header value
			for _, val := range currheader {
				cntValFound++
				if cntValFound > 1 {
					retval = append(retval, []byte("; ")...)
				}
				retval = append(retval, []byte(val)...)
			}
		}
	}
	if cntValFound > 1 { //cut ending ' '
		retval = retval[:len(retval)-1]
	}
	return retval
}
