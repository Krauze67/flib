// Copyright 2011 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package proxy

import (
	"crypto/tls"
	"log"
	"net"
	"time"
)

type ssl struct {
	forward Dialer
	config  *tls.Config
	timeout time.Duration
	logger  *log.Logger
	step    *string
}

var emptyConfig tls.Config

func defaultConfig() *tls.Config {
	return &emptyConfig
}

func NewSSL(forward Dialer, config *tls.Config, timeout time.Duration) Dialer {
	return &ssl{
		forward: forward,
		config:  config,
		timeout: timeout,
	}
}
func (s *ssl) SetLogger(logger *log.Logger) Dialer {
	s.logger = logger
	s.forward.SetLogger(logger)
	return s
}

func (s *ssl) Dial(network, addr string) (net.Conn, error) {
	config := s.config

	conn, err := s.forward.Dial(network, addr)
	if err != nil {
		return nil, err
	}

	closeConn := &conn

	defer func() {
		if closeConn != nil {
			(*closeConn).Close()
		}
	}()

	if config == nil {
		config = defaultConfig()
	}

	// If no ServerName is set, infer the ServerName
	// from the hostname we're connecting to.
	if config.ServerName == "" {
		host, _, _ := net.SplitHostPort(addr)

		// Make a copy to avoid polluting argument or default.
		c := *config
		c.ServerName = host
		config = &c
	}

	connSSL := tls.Client(conn, config)

	if s.logger != nil {
		s.logger.Println("SSL handshake started")
	}

	if s.timeout != 0 {
		connSSL.SetDeadline(time.Now().Add(s.timeout))
	}

	if err := connSSL.Handshake(); err != nil {
		return nil, NewError(err, "CONNECT:SSL")
	}

	connSSL.SetDeadline(time.Time{})

	if s.logger != nil {
		s.logger.Println("SSL handshake success")
	}

	closeConn = nil
	return connSSL, nil
}
