package proxy

import (
	"errors"
	"io"
	"log"
	"net"
	"strconv"
	"time"
)

// SOCKS5 returns a Dialer that makes SOCKSv5 connections to the given address
// with an optional username and password. See RFC 1928.
func SOCKS5(network, addr string, auth *Auth, forward Dialer, timeout time.Duration) Dialer {
	s := &socks5{
		network: network,
		addr:    addr,
		forward: forward,
		timeout: timeout,
	}
	if auth != nil {
		s.user = auth.User
		s.password = auth.Password
	}

	return s
}

type socks5 struct {
	user, password string
	network, addr  string
	forward        Dialer
	timeout        time.Duration
	logger         *log.Logger
}

const socks5Version = 5

const (
	socks5AuthNone     = 0
	socks5AuthPassword = 2
)

const socks5Connect = 1

const (
	socks5IP4    = 1
	socks5Domain = 3
	socks5IP6    = 4
)

var socks5Errors = []string{
	"",
	"general failure",
	"connection forbidden",
	"network unreachable",
	"host unreachable",
	"connection refused",
	"TTL expired",
	"command not supported",
	"address type not supported",
}

func (s *socks5) logPrintf(format string, v ...interface{}) {
	if s.logger != nil {
		s.logger.Printf(format, v...)
	}
}

func (s *socks5) logPrintln(v ...interface{}) {
	if s.logger != nil {
		s.logger.Println(v...)
	}
}

func (s *socks5) logIn(v []byte) {
	if s.logger != nil {
		s.logger.Printf("S: % x\n", v)
	}
}

func (s *socks5) logOut(v []byte) {
	if s.logger != nil {
		s.logger.Printf("C: % x\n", v)
	}
}

func (s *socks5) getUsableIP(ips []net.IP) net.IP {
	if len(ips) < 1 {
		return nil
	}

	for _, ip := range ips {
		if ip4 := ip.To4(); ip4 != nil {
			return ip4
		}
	}
	return nil
}

func (s *socks5) SetLogger(logger *log.Logger) Dialer {
	s.logger = logger
	s.forward.SetLogger(logger)
	return s
}

// Dial connects to the address addr on the network net via the SOCKS5 proxy.
func (s *socks5) Dial(network, addr string) (net.Conn, error) {
	var conn net.Conn
	var ip net.IP
	var port int

	switch network {
	case "tcp", "tcp6", "tcp4":
	default:
		return nil, errors.New("proxy: no support for SOCKS5 proxy connections of type " + network)
	}

	if host, portStr, err := net.SplitHostPort(addr); err != nil {
		return nil, NewError(err, "CONNECT:SOCKS5:ADDRPARSE")
	} else {
		if p, err := strconv.Atoi(portStr); err != nil {
			return nil, NewError(errors.New("failed to parse port number: "+portStr), "CONNECT:SOCKS5:ADDRPARSE")
		} else {
			if p < 1 || p > 0xffff {
				return nil, NewError(errors.New("port number out of range: "+portStr), "CONNECT:SOCKS5:ADDRPARSE")
			} else {
				port = p
			}
		}

		if ips, err := net.LookupIP(host); err != nil {
			return nil, NewError(err, "CONNECT:SOCKS5:RESOLVE")
		} else if ip = s.getUsableIP(ips); ip == nil {
			return nil, NewError(errors.New("No usable address resolved"), "CONNECT:SOCKS5:RESOLVE")
		}
	}

	conn, err := s.forward.Dial(s.network, s.addr)
	if err != nil {
		return nil, err
	}

	if s.timeout != 0 {
		conn.SetDeadline(time.Now().Add(s.timeout))
	}

	closeConn := &conn

	defer func() {
		if closeConn != nil {
			(*closeConn).Close()
		}
	}()

	// the size here is just an estimate
	buf := make([]byte, 0, 6+256)

	buf = append(buf, socks5Version)
	if len(s.user) > 0 && len(s.user) < 256 && len(s.password) < 256 {
		buf = append(buf, 2 /* num auth methods */, socks5AuthNone, socks5AuthPassword)
	} else {
		buf = append(buf, 1 /* num auth methods */, socks5AuthNone)
	}

	if _, err := conn.Write(buf); err != nil {
		return nil, NewError(err, "CONNECT:SOCKS5:GREETING:WRITE")
	}
	s.logOut(buf)

	if _, err := io.ReadFull(conn, buf[:2]); err != nil {
		return nil, NewError(err, "CONNECT:SOCKS5:GREETING:READ")
	}

	s.logIn(buf[:2])

	if buf[0] != 5 {
		return nil, NewError(errors.New("SOCKS5 proxy has unexpected version "+strconv.Itoa(int(buf[0]))), "CONNECT:SOCKS5:GREETING:READ")
	}
	if buf[1] == 0xff {
		return nil, NewError(errors.New("SOCKS5 proxy requires authentication"), "CONNECT:SOCKS5:GREETING:READ")
	}

	if buf[1] == socks5AuthPassword {
		buf = buf[:0]
		buf = append(buf, 1 /* password protocol version */)
		buf = append(buf, uint8(len(s.user)))
		buf = append(buf, s.user...)
		buf = append(buf, uint8(len(s.password)))
		buf = append(buf, s.password...)

		if _, err := conn.Write(buf); err != nil {
			return nil, NewError(err, "CONNECT:SOCKS5:AUTH:WRITE")
		}

		if _, err := io.ReadFull(conn, buf[:2]); err != nil {
			return nil, NewError(err, "CONNECT:SOCKS5:AUTH:READ")
		}

		if buf[1] != 0 {
			return nil, NewError(errors.New("SOCKS5 proxy rejected username/password"), "CONNECT:SOCKS5:AUTH:READ")
		}
	}

	buf = buf[:0]
	buf = append(buf, socks5Version, socks5Connect, 0 /* reserved */)

	/*
		if ip := net.ParseIP(host); ip != nil {
			if ip4 := ip.To4(); ip4 != nil {
				buf = append(buf, socks5IP4)
				ip = ip4
			} else {
				buf = append(buf, socks5IP6)
			}
			buf = append(buf, ip...)
		} else {
			if len(host) > 255 {
				if s.step != nil {
					*s.step = "SOCKS5WriteConnect"
				}
				return nil, errors.New("proxy: destination hostname too long: " + host)
			}
			buf = append(buf, socks5Domain)
			buf = append(buf, byte(len(host)))
			buf = append(buf, host...)
		}
	*/

	buf = append(buf, socks5IP4)
	buf = append(buf, ip...)
	buf = append(buf, byte(port>>8), byte(port))

	if _, err := conn.Write(buf); err != nil {
		return nil, NewError(err, "CONNECT:SOCKS5:CONNECT:WRITE")
	}

	s.logOut(buf[:2])

	if _, err := io.ReadFull(conn, buf[:4]); err != nil {
		return nil, NewError(err, "CONNECT:SOCKS5:CONNECT:READ")
	}

	s.logIn(buf[:4])

	failure := "unknown error"
	if int(buf[1]) < len(socks5Errors) {
		failure = socks5Errors[buf[1]]
	}

	if len(failure) > 0 {
		return nil, NewError(errors.New("SOCKS5 connect answer: "+failure), "CONNECT:SOCKS5:CONNECT:READ")
	}

	bytesToDiscard := 0
	switch buf[3] {
	case socks5IP4:
		bytesToDiscard = net.IPv4len
	case socks5IP6:
		bytesToDiscard = net.IPv6len
	case socks5Domain:
		_, err := io.ReadFull(conn, buf[:1])
		if err != nil {
			return nil, NewError(err, "CONNECT:SOCKS5:CONNECT:READ")
		}
		bytesToDiscard = int(buf[0])
		s.logPrintf("IN:  % x\n", buf[:1])
	default:
		return nil, NewError(errors.New("got unknown address type "+strconv.Itoa(int(buf[3]))+" from SOCKS5 proxy"), "CONNECT:SOCKS5:CONNECT:READ")
	}

	if cap(buf) < bytesToDiscard {
		buf = make([]byte, bytesToDiscard)
	} else {
		buf = buf[:bytesToDiscard]
	}
	if _, err := io.ReadFull(conn, buf); err != nil {
		return nil, NewError(err, "CONNECT:SOCKS5:CONNECT:READ")
	}

	s.logIn(buf)

	// Also need to discard the port number
	if _, err := io.ReadFull(conn, buf[:2]); err != nil {
		return nil, NewError(err, "CONNECT:SOCKS5:CONNECT:READ")
	}

	s.logIn(buf)

	if s.timeout != 0 {
		conn.SetDeadline(time.Time{})
	}

	closeConn = nil
	return conn, nil
}
