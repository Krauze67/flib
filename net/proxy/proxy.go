// Copyright 2011 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package proxy provides support for a variety of protocols to proxy network
// data.
package proxy

import (
	"crypto/tls"
	"log"
	"net"
	"time"
)

// A Dialer is a means to establish a connection.
type Dialer interface {
	// Dial connects to the given address via the proxy.
	Dial(network, addr string) (c net.Conn, err error)
	SetLogger(logger *log.Logger) Dialer
}

// Auth contains authentication parameters that specific Dialers may require.
type Auth struct {
	User, Password string
}

func DialEx(network, address, sock5addr string, useSSL bool, timeout time.Duration, logger *log.Logger) (net.Conn, error) {
	var dialer Dialer

	switch {
	case sock5addr != "" && useSSL:
		dialer = NewSSL(SOCKS5(network, sock5addr, nil, NewDirect(timeout), timeout), &tls.Config{InsecureSkipVerify: true}, timeout)
	case sock5addr != "" && !useSSL:
		dialer = SOCKS5(network, sock5addr, nil, NewDirect(timeout), timeout)
	case useSSL:
		dialer = NewSSL(NewDirect(timeout), &tls.Config{InsecureSkipVerify: true}, timeout)
	default:
		dialer = NewDirect(timeout)
	}

	if logger != nil {
		return dialer.SetLogger(logger).Dial(network, address)
	}

	return dialer.Dial(network, address)
}

type OpError struct {
	Err  error
	Step string
}

func (err *OpError) Error() string {
	return err.Err.Error()
}

func NewError(err error, step string) error {
	if err == nil {
		return nil
	}

	if e, ok := err.(*net.OpError); ok {
		err = e.Err
	}

	if e, ok := err.(*net.OpError); ok {
		err = e.Err
	}

	return &OpError{Err: err, Step: step}
}
