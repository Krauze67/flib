package proxy

import (
	"log"
	"net"
	"time"
)

type direct struct {
	timeout time.Duration
	logger  *log.Logger
}

// Direct is a direct proxy: one that makes network connections directly.
var Direct = NewDirect(0)

func NewDirect(timeout time.Duration) Dialer {
	return &direct{timeout: timeout}
}

func (d *direct) SetLogger(logger *log.Logger) Dialer {
	d.logger = logger
	return d
}

func (d *direct) Dial(network, addr string) (net.Conn, error) {
	if d.logger != nil {
		d.logger.Println("Connecting to ", network, " ", addr)
	}

	if d.timeout == 0 {
		conn, err := net.Dial(network, addr)

		if err == nil && d.logger != nil {
			d.logger.Println("Succsess connected to ", network, " ", addr)
		}

		return conn, NewError(err, "CONNECT:DIRECT")
	}

	conn, err := net.DialTimeout(network, addr, d.timeout)

	if err == nil && d.logger != nil {
		d.logger.Println("Succsess connected to ", network, " ", addr)
	}

	return conn, NewError(err, "CONNECT:DIRECT")
}
