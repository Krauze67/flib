package slicetools

func AddIfNewString(pdest *[]string, newItem string) bool {
	for _, item := range *pdest {
		if newItem == item {
			return false
		}
	}
	(*pdest) = append((*pdest), newItem)
	return true
}

func AddIfNewInt64(pdest *[]int64, newItem int64) bool {
	for _, item := range *pdest {
		if newItem == item {
			return false
		}
	}
	(*pdest) = append((*pdest), newItem)
	return true
}

func AddIfNewFloat64(pdest *[]float64, newItem float64) bool {
	for _, item := range *pdest {
		if newItem == item {
			return false
		}
	}
	(*pdest) = append((*pdest), newItem)
	return true
}

func AppendUniqueString(pdest *[]string, newItems []string) int64 {
	var addednew int64
	for _, newitem := range newItems {
		if AddIfNewString(pdest, newitem) {
			addednew++
		}
	}
	return addednew
}

func AppendUniqueInt64(pdest *[]int64, newItems []int64) int64 {
	var addednew int64
	for _, newitem := range newItems {
		if AddIfNewInt64(pdest, newitem) {
			addednew++
		}
	}
	return addednew
}

func AppendUniqueFloat64(pdest *[]float64, newItems []float64) int64 {
	var addednew int64
	for _, newitem := range newItems {
		if AddIfNewFloat64(pdest, newitem) {
			addednew++
		}
	}
	return addednew
}
